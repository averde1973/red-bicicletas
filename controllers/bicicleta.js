var Bicicleta = require('../models/bicicleta');

const { findById } = require('../models/bicicleta'); // ADDED

//------------------------------------
exports.bicicleta_list = function(req, res){
    res.render('bicicletas/index', {bicis: Bicicleta.allBicis});

    // Bicicleta.allBicis(function(err,bicis){            // ADDED
    //     res.render('bicicletas/index',{bicis: bicis});
    // });
}
//------------------------------------


//------------------------------------
exports.bicicleta_create_get = function(req, res){
    res.render('bicicletas/create');
}
//------------------------------------


//------------------------------------
exports.bicicleta_create_post = function(req, res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);

    res.redirect('/bicicletas');

    // var bici= new Bicicleta({        // ADDED
    //     code:req.body.code,
    //     color:req.body.color,
    //     modelo:req.body.modelo,
    //     ubicacion:[req.body.latitud, req.body.longitud]
    // });
    // bici.save(function (err) {
    // if (err) return handleError(err);
    // res.redirect('/bicicletas');
    // });
}
//------------------------------------

//------------------------------------
// exports.bicicleta_update_get=function (req,res) {             // ADDED
//     Bicicleta.findByCode(req.params.code,function(err,bici){
//             res.render('bicicletas/update', {bici:bici});
        
//     });
    
// }

exports.bicicleta_update_get = function(req, res){
    var bici = Bicicleta.findById(req.params.id);

    res.render('bicicletas/update', {bici});
}
//------------------------------------


//------------------------------------
// exports.bicicleta_update_post=function (req,res) {  // ADDED

//     Bicicleta.update();
//     res.redirect('/bicicletas');
// }

exports.bicicleta_update_post = function(req, res){
    //var bici = Bicicleta(req.body.id, req.body.color, req.body.modelo);
    var bici = Bicicleta.findById(req.params.id);
    bici.id = req.body.id;
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    
    res.redirect('/bicicletas');
}
//------------------------------------


//------------------------------------
exports.bicicleta_delete_post = function(req, res) {
    Bicicleta.removeById(req.body.id);

    res.redirect('/bicicletas');

    // Bicicleta.deleteOne({ code: req.body.code }, function(err, result) {  // ADDED
    //     if (err) {
    //       res.send(err);
    //     } else {
    //       res.redirect('/bicicletas');
    //     }
    //   });
}
//------------------------------------
var Bicicleta = require('../../models/bicicleta');
const { bicicleta_create_get } = require('../bicicleta'); // ADDED

exports.bicicleta_list = function(req, res) {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });

    // Bicicleta.allBicis(function(err,bicis){  // ADDED
    //     res.status(200).json({
    //         bicicletas: bicis
    //     });
    // });

}

exports.bicicleta_create = function(req, res){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Bicicleta.add(bici);

    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, res){
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}

//-----------------------------------------
// ADDED
exports.bicicleta_edit = function (req,res) {
    var bici = Bicicleta.findBycode(req.body.code);
    bici.id     = req.body.id;
    bici.color  = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat,req.body.lng];

    res.status(204).send();
}
//-----------------------------------------
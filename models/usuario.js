var mongoose = require('mongoose');
var Reserva = require('./reserva');

const bcrypt = require('bcrypt');
const saltRounds = 10;

//var Reserva = require('../models/reserva'); // ADDED
//var moment  = require('moment');            // ADDED
//const { response } = require('express');    // ADDED
var Schema = mongoose.Schema;

const validateEmail = function(email) {
    //const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*[\.\w{2,3})+$/;
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
};

var usuarioSchema = new Schema({
    //nombre: String,
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        validate: [validateEmail, 'Por favor, ingrese un email valido'],
        match: [/\S+@\S+\.\S+/]
    },
    password: {
        type: String,
        required: [true, 'El password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    }
});


usuarioSchema.pre('save', function(next) {
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    //var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta, cb: cb});
    //var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta}); // ADDED
    console.log(reserva);
    reserva.save(cb);
}

module.exports = mongoose.model('Usuario', usuarioSchema);
// module.exports=mongoose.model('Usuario',usuarioSchema,'usuarios'); ADDED